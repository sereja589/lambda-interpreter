module Lambda where

lambdaSymbol = '\\'

data LambdaExpr = Variable String | Abstraction String LambdaExpr | Application LambdaExpr LambdaExpr deriving (Eq)

instance Show LambdaExpr where
    show (Variable s) = s
    show (Abstraction s e) = showAbstraction (Abstraction s e) False
    show (Application x y) = "(" ++ (show x) ++ ")" ++ (show y)

showAbstraction :: LambdaExpr -> Bool -> String
showAbstraction (Abstraction s e) True = " " ++ s ++ (showAbstraction e True)
showAbstraction (Abstraction s e) False = lambdaSymbol : (s ++ showAbstraction e True)
showAbstraction expr True = "." ++ (show expr)
