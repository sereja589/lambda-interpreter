module LambdaParser where

import Lambda
import Text.ParserCombinators.Parsec


lambdaExpr :: Parser LambdaExpr
lambdaExpr = (lambdaAbstraction <|> lambdaApplication <|> topLevelElem)


lambdaVar :: Parser String
lambdaVar = do
    var <- many1 (letter <|> digit)
    spaces
    return var


lambdaAbstraction :: Parser LambdaExpr
lambdaAbstraction = do
    char lambdaSymbol
    spaces
    vars <- many1 lambdaVar
    spaces
    char '.'
    spaces
    expr <- lambdaExpr
    spaces
    return (foldr Abstraction expr vars)


lambdaApplication :: Parser LambdaExpr
lambdaApplication = do
    terms <- many1 topLevelElem
    return (foldl1 Application terms)


topLevelElem :: Parser LambdaExpr
topLevelElem = do
    result <- ((lambdaVar >>= return . Variable) <|> exprInParentheses <|> lambdaAbstraction)
    spaces
    return result


exprInParentheses :: Parser LambdaExpr
exprInParentheses = do
    char '('
    expr <- lambdaExpr
    char ')'
    spaces
    return expr


parseLambda :: String -> Either String LambdaExpr
parseLambda s = case (parse lambdaExpr "lambda expression parsing" s) of
    Left err -> Left $ show err
    Right expr -> Right expr
