module Cli
    ( runCli
    ) where


import System.IO
import LambdaParser
import LambdaInterpreter
import Lambda


printResult :: [LambdaExpr] -> IO()
printResult [] = do return ()
printResult (h:t) = do
    putStrLn $ show h
    printResult t


cliLoop :: IO ()
cliLoop = do
    putStr ">>>"
    hFlush stdout
    finish <- isEOF
    if finish then do
        putStrLn "\nLeaving lambda interpreter"
    else do
        input <- getLine
        if null input
            then return ()
            else do
                let parsedExpr = parseLambda input
                case parsedExpr of
                    Left err -> do
                        putStrLn $ "Error in expression " ++ err
                    Right expr -> do
                        let reduceResult = reduceLambda expr
                        printResult reduceResult
        cliLoop

runCli :: IO ()
runCli = do
    cliLoop
