module LambdaInterpreter where

import Lambda
import Data.Char
import Data.List


freeVariables :: LambdaExpr -> [String] -> [String]
freeVariables (Variable var) bounded | var `elem` bounded = []
                                     | otherwise = [var]
freeVariables (Application expr1 expr2) bounded = freeVariables expr1 bounded `union` freeVariables expr2 bounded
freeVariables (Abstraction var expr) bounded = freeVariables expr (var:bounded)


newName oldName deprecatedNames = head $ filter (`notElem` deprecatedNames) [prefixName ++ show i | i <- [0..]]
    where prefixName = reverse $ dropWhile isDigit (reverse oldName)


rename (Variable var) old new | var == old = Variable new
                              | otherwise = Variable old
rename (Application expr1 expr2) old new = (Application (rename expr1 old new) (rename expr2 old new))
rename (Abstraction var expr) old new | var == old = (Abstraction var expr)
                                      | otherwise = (Abstraction var (rename expr old new))


substitute :: LambdaExpr -> String -> LambdaExpr -> LambdaExpr
substitute src var substituteExpr = case src of
    varExpr@(Variable var2) | var == var2 -> substituteExpr
                            | otherwise -> varExpr
    Application e1 e2 -> Application (substitute e1 var substituteExpr) (substitute e2 var substituteExpr)
    abstractionExpr@(Abstraction var2 expr) | var == var2 -> abstractionExpr
                                            | var2 `elem` fv -> Abstraction name (substitute (rename expr var2 name) var substituteExpr)
                                            | otherwise -> Abstraction var2 (substitute expr var substituteExpr)
                                            where fv = freeVariables substituteExpr []
                                                  name = newName var2 (fv `union` (freeVariables expr []))


reductionStep :: LambdaExpr -> (LambdaExpr, Bool)
reductionStep (Application (Abstraction var expr1) expr2) = ((substitute expr1 var expr2), True)
reductionStep (Variable var) = ((Variable var), False)
reductionStep (Application expr1 expr2) = case (reductionStep expr1) of
    (result1, True) -> ((Application result1 expr2), True)
    (result1, False) -> ((Application expr1 result2), success2)
        where (result2, success2) = reductionStep expr2
reductionStep (Abstraction var expr) = ((Abstraction var result), success)
    where (result, success) = reductionStep expr


data ReductionStep = LambdaExpr


reduceLambda :: LambdaExpr -> [LambdaExpr]
reduceLambda expr = reduceLambdaImpl expr []


reduceLambdaImpl :: LambdaExpr -> [LambdaExpr] -> [LambdaExpr]
reduceLambdaImpl expr previous = case (reductionStep expr) of
    (result, True) -> if result `notElem` nextPrevious then reduceLambdaImpl result nextPrevious else (result:nextPrevious)
    (result, False) -> reverse nextPrevious
    where nextPrevious = (expr:previous)
