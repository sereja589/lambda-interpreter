import Test.HUnit
import LambdaInterpreter
import LambdaParser
import Lambda

test1 :: Test
test1 = TestCase $ assertEqual "Failed" "v" (show $ Variable "v")

test2 :: Test
test2 = TestCase $ assertEqual "Failed" "\\xx yy zz.(xx)yy" (show $ Abstraction "xx" (Abstraction "yy" (Abstraction "zz" (Application (Variable "xx") (Variable "yy")))))

test3 :: Test
test3 = TestCase $ assertEqual "Failed" (parseLambda "\\x.y") (Right (Abstraction "x" $ Variable "y"))

test4 :: Test
test4 = TestCase $ assertEqual "Failed" (parseLambda "x") (Right (Variable "x"))

test5 :: Test
test5 = TestCase $ assertEqual "Failed" (parseLambda "(\\x.y)y") (Right (Application (Abstraction "x" $ Variable "y") (Variable "y")))

test6 :: Test
test6 = TestCase $ assertEqual "Failed" (parseLambda "x y") (Right (Application (Variable "x") (Variable "y")))

test7 :: Test
test7 = TestCase $ assertEqual "Failed" (parseLambda "\\x.y z") (Right (Abstraction "x" (Application (Variable "y") (Variable "z"))))

reduce :: String -> String
reduce textExpr = case parseLambda textExpr of
    Left err -> error ("Failed to parse expr: " ++ err)
    Right expr -> show $ head . reverse $ reduceLambda expr

test8 :: Test
test8 = TestCase $ assertEqual "Failed" (reduce "(\\x.x) \\y.y") "\\y.y"

test9 :: Test
test9 = TestCase $ assertEqual "Failed" (reduce "(\\x.x)(\\y.y)(\\x.x y z)") "\\x.((x)y)z"

test10 :: Test
test10 = TestCase $ assertEqual "Failed" (reduce "\\x y.(\\x.x)(\\y.y)(\\x.x y z)") "\\x y x.((x)y)z"

test11 :: Test
test11 = TestCase $ assertEqual "Failed" (reduce "\\x.((\\x.x) y)") "\\x.y"

main :: IO Counts
main = do
    _ <- runTestTT test1
    _ <- runTestTT test2
    _ <- runTestTT test3
    _ <- runTestTT test4
    _ <- runTestTT test5
    _ <- runTestTT test6
    _ <- runTestTT test7
    _ <- runTestTT test8
    _ <- runTestTT test9
    _ <- runTestTT test10
    runTestTT test11
